package com.example.acer_pc.secondtask;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ListViewActivity extends AppCompatActivity {

    ListView List;
    ArrayList contacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listviewactivity_main);

        List = (ListView) (findViewById(R.id.list1));
        contacts = new ArrayList<String>();
            contacts.add("John Doe");
            contacts.add("Travis Smith");
            contacts.add("Chris Hemsworth");
            contacts.add("Louie Gomez");
            contacts.add("Jane Garde");
            contacts.add("Leah Duco");
            contacts.add("Luke Pelongco");
            contacts.add("Zac Efron");
            contacts.add("Ryan Guzman");
            contacts.add("Chloe Moretz");

        ArrayAdapter<String> namesAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, contacts);
        List.setAdapter(namesAdapter);

    }

}
