package com.example.acer_pc.secondtask;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.ArrayList;

public class ClickableListView extends AppCompatActivity {

    EditText enterName;
    ListView clickList;
    Button addName;
    String inputHolder;
    ArrayList<String> itemList;
    ArrayAdapter<String> showInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_exercise);

        enterName = (EditText) (findViewById(R.id.et_name));
        clickList = (ListView) (findViewById(R.id.listView_nameList));
        addName = (Button) (findViewById(R.id.add_button));
        itemList = new ArrayList<String>();
//-------------------------Add name to the list view when ADD button is clicked-------------------------------------------
        addName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((enterName != null && enterName.getText().length() != 0)) {
                    itemList.add(enterName.getText().toString());
                     showInput = new ArrayAdapter<String>(ClickableListView.this, android.R.layout.simple_list_item_1, itemList);
                    enterName.setText("");
                    clickList.setAdapter(showInput);

                } else {
                    YoYo.with(Techniques.Shake)
                            .duration(3000)
                            .playOn(enterName);
                }

            }


        });
//------------------------Set item click listener to the list view items----------------------------
        clickList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                inputHolder = clickList.getItemAtPosition(position).toString();//<<-----Stored the name from the listview to the String inputHolder
                alertDialog(inputHolder).show(); //<<<<<-----Called the method to show the alertdialog, sending the inputHolder value

            }
        });


    }


    //-----------------Created new Method for the Alert Dialog...All functions within the dialog are here------------------------
    private AlertDialog alertDialog(final String nameFromListView) { //<<<------the nameFromListView now contains the value of inputHolder

        AlertDialog.Builder builder = new AlertDialog.Builder(ClickableListView.this);
        LayoutInflater inflater = ClickableListView.this.getLayoutInflater();
        View dialog = inflater.inflate(R.layout.dialoglayout, null);
        builder.setView(dialog);

        final AlertDialog alert_dialog = builder.create();
        alert_dialog.setCancelable(true);


        Button saveButton = (Button) dialog.findViewById(R.id.saveButton);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);

        final EditText input = (EditText) dialog
                .findViewById(R.id.input);

        input.setText(nameFromListView);//---------------Set the text in the EditText to the name from the ListView which was store to the string and passed to the nameFromListView
//-----------------Cancel button functions-------------------------------
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert_dialog.hide();//<-----Will hide the dialog------------
            }
        });

//------------------Save button function when clicked--------------------
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (input.getText().toString() != null) {
                    itemList.set(itemList.indexOf(nameFromListView), input.getText().toString());//<<----updated the name on the arraylist by finding the index of the original name and updating the value to the new one
                    showInput = new ArrayAdapter<String>(ClickableListView.this, android.R.layout.simple_list_item_1, itemList);///<-----function to refresh listview--------------------------
                    clickList.setAdapter(showInput);///<-----function to refresh listview--------------------------
                    alert_dialog.hide();
                    Toast.makeText(ClickableListView.this, "You successfully updated the name to: "+ input.getText().toString(), Toast.LENGTH_LONG).show();

                }


            }


        });

        return alert_dialog;

    }

}
