package com.example.acer_pc.secondtask;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    EditText email, password;
    Button send;
    String emailIn, pass;
    ArrayList<String> usersArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginactivity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        email = (EditText) (findViewById(R.id.loginEmail));
    //    password = (EditText) (findViewById(R.id.loginPass));
        send = (Button) (findViewById(R.id.loginButton));
        usersArray = new ArrayList<String>();
            usersArray.add("Admin");
            usersArray.add("Supervisor");
            usersArray.add("Manager");
            usersArray.add("User");


        send.setOnClickListener (new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if ((email!=null && email.getText().length()!=0)){
                        emailIn = email.getText().toString();
                    if (usersArray.contains(emailIn)) {
                        Intent loginDetails = new Intent(LoginActivity.this, MainActivity.class);
                        loginDetails.putExtra("emailDits", emailIn);
                        startActivity(loginDetails);
                        finish();
                    }
                    else
                        Toast.makeText(LoginActivity.this, "Username cannot be identified.", Toast.LENGTH_LONG).show();

                }
                else {
                    YoYo.with(Techniques.Shake)
                            .duration(3000)
                            .playOn(email);
                //    YoYo.with(Techniques.Shake)
                //            .duration(3000)
                //            .playOn(password);
                }


            }
        });

    }

}
