package com.example.acer_pc.secondtask;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView emailShow;
    Button toListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toListView = (Button) (findViewById(R.id.toListView));
        //id of textview
        emailShow = (TextView) (findViewById(R.id.mainEmailShow));
        Intent getDetails = getIntent();

        //account user: + username
        String emailGet = (String) getDetails.getSerializableExtra("emailDits");
        emailShow.setText(emailGet);

        toListView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                Intent ListViewIntent = new Intent(MainActivity.this, ClickableListView.class);
                startActivity(ListViewIntent);
                finish();

            }



        });

    }

}

