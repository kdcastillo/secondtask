package com.example.acer_pc.secondtask;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.w3c.dom.Text;

public class SplashScreen extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashactivity_main);

        text = (TextView) (findViewById(R.id.splashTV1));

        YoYo.with(Techniques.Pulse)
                .duration(3000)
                .playOn(text);


        Thread logoTimer = new Thread() {
            @Override
            public void run() {
               try {
                   sleep(4000);
                   Intent i = new Intent("android.intent.action.LOGIN");
                   startActivity(i);
               }
               catch (InterruptedException e) {
                   e.printStackTrace();
               }
               finally {
                   finish();

               }
            }
        };
            logoTimer.start();

    }

}
